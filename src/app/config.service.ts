import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  title = "Sendgrid Email";
  summary = "This extension provides functionality to send plain text and dynamic template emails with Sendgrid.";
  editText = "Get source code";
  editLink = "https://gitlab.com/xano-marketplace/xano-sendgrid-email";
  descriptionHtml = `
  <h4>Description</h4>
  <p>Sendgrid is one of the premiere email service providers that provides a ton of features that make it easy to maintain the emails that you send. </p>
  <p>This extension includes two functions to send either a basic plain text or a dynamic template email. Plain text emails are useful for debugging and simple notifications. Dynamic template emails allow more robust rich text solutions where data can be dynamically injected into the contents of the email.</p>
  `;
  logoHtml = '<svg xmlns="http://www.w3.org/2000/svg" style="max-width:100%" width="600" height="105.3" viewBox="0 0 600 105.3"> <g id="sendgrid-vector-logo" transform="translate(-20.3 -268.9)"> <path id="Path_1" data-name="Path 1" d="M55,268.9v34.7H20.3V373H89.7V338.3h34.7V268.9Z" fill="#fff"/> <path id="Path_2" data-name="Path 2" d="M20.3,303.6v0Zm0,0h0Z" fill="#aee1f3"/> <path id="Path_3" data-name="Path 3" d="M20.3,303.6v34.7H55V373H89.7V338.3H55V303.6Z" fill="#fff"/> <path id="Path_4" data-name="Path 4" d="M20.3,303.6v34.7H55V373H89.7V338.3H55V303.6Z" fill="#aee1f3"/> <g id="Group_1" data-name="Group 1"> <path id="Path_5" data-name="Path 5" d="M143.9,358.6,159,346.7c4.3,7.4,11.1,11.6,18.8,11.6,8.4,0,13-5.5,13-11.4,0-7.1-8.7-9.4-17.9-12.2-11.6-3.6-24.6-8.1-24.6-24.7,0-13.9,12.1-25,28.9-25,14.1,0,22.2,5.3,29.2,12.6L192.6,308a17.738,17.738,0,0,0-15.5-8.2c-7.7,0-11.9,4.2-11.9,9.6,0,6.7,8.3,8.9,17.6,12,11.8,3.8,25.1,9,25.1,25.7,0,13.8-10.9,27.1-30.1,27.1-15.6,0-26.1-6.7-33.9-15.6m132.9-45H293v6.7a20.3,20.3,0,0,1,16.4-7.8c14,0,22.5,9,22.5,24.4V373H315.4V338.9c0-8-3.7-12.6-10.9-12.6-6.2,0-11.3,4.3-11.3,14.3V373H276.9V313.6Zm58.8,29.7c0-21,15.4-30.9,28.9-30.9,7.7,0,13.8,2.9,17.7,7V286.2h16.3V373H382.2v-6.7a23.154,23.154,0,0,1-17.9,7.8c-12.6.1-28.7-9.9-28.7-30.8m47.1-.1c0-8.8-6.4-15.9-15.2-15.9a15.95,15.95,0,0,0,0,31.9c8.8,0,15.2-7.2,15.2-16m19.6-13.6c0-24.7,18.7-44.6,44.3-44.6,12.8,0,23.6,4.6,31.5,12a43.41,43.41,0,0,1,8,10.2L471.6,316c-5.3-9.9-13.5-15-24.8-15a28.274,28.274,0,0,0-28.1,28.5c0,15.9,12,28.5,28.6,28.5,12.6,0,21.5-7.1,24.6-18.2H444.7V324h45v6.7c0,23.3-16.6,43.4-42.4,43.4-27,.1-45-20.5-45-44.5m91.6-16h16.2v9.7c3-6.2,8.3-9.7,16.4-9.7H533l-5.9,15.8h-4.4c-8.7,0-12.5,4.5-12.5,15.6v28H493.9V313.6Zm43.4,0h16.3V373H537.3V329.4h-5.9Zm8.1-9.1a9.7,9.7,0,1,0-9.7-9.7,9.709,9.709,0,0,0,9.7,9.7m12,38.8c0-21,15.4-30.9,28.9-30.9,7.7,0,13.8,2.9,17.7,7V286.2h16.3V373H604v-6.7a23.154,23.154,0,0,1-17.9,7.8c-12.7.1-28.7-9.9-28.7-30.8m47.1-.1c0-8.8-6.4-15.9-15.2-15.9a15.95,15.95,0,0,0,0,31.9c8.8,0,15.2-7.2,15.2-16m-331.5,0c0-17.1-12.5-30.8-30.7-30.8a30.856,30.856,0,0,0-30.9,30.9c0,17.1,12.8,30.9,31.4,30.9,12.8,0,22.1-6.2,27.3-15.1l-12.9-7.7a15.576,15.576,0,0,1-14.2,8.5c-8.6,0-13.9-4.3-15.8-10.8H273ZM227.8,336a15.5,15.5,0,0,1,14.6-9.4c6.9,0,12,3.2,14.1,9.4Z" fill="#263746"/> <rect id="Rectangle_1" data-name="Rectangle 1" width="34.7" height="34.7" transform="translate(20.3 338.3)" fill="#2e7bbf"/> <path id="Path_6" data-name="Path 6" d="M89.7,268.9h0Zm34.7,69.4h0v0" fill="#00b3e3"/> <path id="Path_7" data-name="Path 7" d="M124.4,338.3H89.7V303.6H55V268.9H89.7v34.7h34.7Z" fill="#fff"/> <path id="Path_8" data-name="Path 8" d="M124.4,338.3H89.7V303.6H55V268.9H89.7v34.7h34.7Z" fill="#00b3e3"/> <rect id="Rectangle_2" data-name="Rectangle 2" width="34.7" height="34.7" transform="translate(55 303.6)" fill="#fff"/> <rect id="Rectangle_3" data-name="Rectangle 3" width="34.7" height="34.7" transform="translate(55 303.6)" fill="#00b3e3"/> <rect id="Rectangle_4" data-name="Rectangle 4" width="34.7" height="34.7" transform="translate(89.7 268.9)" fill="#2e7bbf"/> </g> </g></svg>';

  requiredApiPaths = [
    '/sendgrid/validate'
  ];

  xanoApiUrl;

  constructor() { }

  isConfigured() {
    return !!this.xanoApiUrl;
  }
}
