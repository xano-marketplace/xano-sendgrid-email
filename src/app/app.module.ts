import { BrowserModule } from '@angular/platform-browser';
import { NgModule, resolveForwardRef } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ConfigComponent } from './config/config.component';
import { NotExistsComponent } from './not-exists/not-exists.component';
import { HomeComponent } from './home/home.component';
import { PanelHeaderComponent } from './panel-header/panel-header.component';
import { PanelInfoComponent } from './panel-info/panel-info.component';
import { PanelFooterComponent } from './panel-footer/panel-footer.component';
import { FormInputComponent } from './form-input/form-input.component';
import { SendgridComponent } from './sendgrid/sendgrid.component';
import { ToastComponent } from './toast/toast.component';
import { SafeHtmlPipe } from './safe-html.pipe';
import { ConfigService } from './config.service';

@NgModule({
  declarations: [
    AppComponent,
    ConfigComponent,
    NotExistsComponent,
    HomeComponent,
    PanelHeaderComponent,
    PanelInfoComponent,
    PanelFooterComponent,
    FormInputComponent,
    SendgridComponent,
    ToastComponent,
    SafeHtmlPipe,
  ],
  imports: [
    NgbModule,
    BrowserModule,
    HttpClientModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
