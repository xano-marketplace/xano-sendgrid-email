import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { faSave, faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-panel-footer',
  templateUrl: './panel-footer.component.html',
  styleUrls: ['./panel-footer.component.scss'],
  host: {'class': 'position-sticky mt-auto'}
})
export class PanelFooterComponent implements OnInit {
  @Output() onSave =  new EventEmitter;
  @Output() onDelete =  new EventEmitter;
  @Output() onGoBack =  new EventEmitter;

  @Input() disabled;
  @Input() deleting;
  @Input() saving;
  @Input() saveType = "button";
  @Input() saveIcon = faSave;
  @Input() saveLabel = "Save";
  @Input() saveClass = "btn ml-auto btn-primary";

  @Input() trashIcon = faTrashAlt;
  @Input() trashLabel = "";

  spinner = faSpinner;

  static ID = Symbol();
  constructor() { }

  ngOnInit() {
  }

  hasSave() {
    return this.onSave.observers.length > 0;
  }

  isSaving() {
    let ret = this.saving;
    while (typeof ret == "function") {
      ret = ret();
    }
    return ret;
  }

  save() {
    this.onSave.emit();
  }

  hasGoBack() {
    return this.onGoBack.observers.length > 0;
  }

  goBack() {
    this.onGoBack.emit();
  }

  hasDelete() {
    return this.onDelete.observers.length > 0;
  }

  delete() {
    this.onDelete.emit();
  }
}
